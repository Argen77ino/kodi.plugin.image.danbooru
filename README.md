# Danbooru-Client for Kodi #
*Copyright (C) 2014  Markus Koch <CClassicVideos@aol.com>*

This project allows you to access Danbooru-based image boards (and its derivatives) from within your [Kodi](http://xbmc.org/)-Installation.

It is the direct successor to the [Moebooru Client](https://bitbucket.org/C_Classic/xbmc.plugin.image.moebooru) for XBMC.

## Status ##
The project is still in pre-alpha phase. We are currently working on the python layer for the API.

## Features (when ready) ##
* Support for:
  * [Moebooru](https://bitbucket.org/edogawaconan/moebooru) such as [Konachan](http://konachan.com/)
  * [Danbooru](https://github.com/r888888888/danbooru) such as [Danbooru.donmai.us](http://danbooru.donmai.us/)
  * [Gelbooru](http://gelbooru.com/), although only very limited (unless they extend their API)
  * Possibly [chan.sankakucomplex.com](http://chan.sankakucomplex.com) if they fix their API
* Browse the latest images
* Search for images using tags (with search history)
* Search for tags
* Slideshows and favorites using the built-in functions of Kodi
* Browse pools

## License ##
This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License version 3 as published by the Free Software Foundation.
This program is distributed in the hope that it will be useful, but **without any warranty**; without even the implied warranty of **merchantability** or **fitness for a particular purpose**.  See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with this program. If not, see [http://www.gnu.org/licenses/](http://www.gnu.org/licenses/).

## Version history ##
### Pre-alpha ###
    2014-08-14  Started implementing the python layer